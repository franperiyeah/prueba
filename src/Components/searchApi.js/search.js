import React, { useState } from "react";
import axios from "axios";
import Tiempo from "../searcher";
import * as ReactBootStrap from "react-bootstrap";
import "./search.scss";

function Search() {
  const [search, setSearch] = useState("");
  const [cities, setCities] = useState([]);
  const [avg, setAvg] = useState("");
  const [orderWeather, setOrderWeather] = useState(1);
  const [isLoading, setLoading] = useState(false);
  const [climas, setClimas] = useState([]);
  const [disable, setDisable] = useState(false);

  // Peticiones a la API usando axios
  const getWeather = () => {
    let promises = [];
    let list = [];
    let temp = [];
    let filteredNames = [];
    axios
      .get(`https://www.metaweather.com/api/location/search/?query=${search}`, {
        headers: { "Access-Control-Allow-Origin": "*" },
      })
      .then((response) => {
        for (const location of response.data) {
          promises.push(
            axios
              .get(
                `https://www.metaweather.com/api/location/${location.woeid}/`, // con el woeid obtenido del primer .get se accede a los datos de la ciudad con otra llamada.
                {
                  headers: { "Access-Control-Allow-Origin": "*" },
                }
              )
              .then((res) => {
                res.data.icon_url = `https://www.metaweather.com/static/img/weather/${res.data.consolidated_weather[0].weather_state_abbr}.svg`;
                res.data.weather = res.data.consolidated_weather[0].the_temp;
                res.data.weatherName =
                  res.data.consolidated_weather[0].weather_state_name;
                filteredNames.push(res.data.weatherName);
                list.push(res.data);
                temp.push(res.data.weather);
                // Aquí realizo la suma de las temperaturas para después proceder al cálculo de su media.
                let total = temp.reduce((sum, current) => sum + current, 0);
                let average = total / temp.length;
                setAvg(average);
                // En este parte se recogen los distintos tipos de clima y se filtran para que no se repitan.
                const clima = filteredNames;
                const newClima = clima.filter(
                  (el, index) => clima.indexOf(el) === index
                );
                setClimas(newClima);
              })
          );
          setLoading(true);
          setDisable(true);
          setTimeout(() => {
            setLoading(false);
            setDisable(false);
          }, 6000);
        }
        Promise.all(promises).then(() => {
          setCities(list);
        });
      });
  };
  console.log(cities)

  // const creado para obtener el orden de las ciudades ordenadas según su temperatura.

  const sortedTemp = () => {
    let order = orderWeather === 1 ? -1 : 1;

    setOrderWeather(order);

    let sortedCitiesByWeather = cities.sort((cityOne, cityTwo) => {
      return orderWeather === 1
        ? cityOne.weather - cityTwo.weather
        : cityTwo.weather - cityOne.weather;
    });

    setCities(sortedCitiesByWeather);
  };

  // Con este evento cogemos el valor del input para realizar la primera llamada a la API.
  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  const handleFilterChange = (e) => {
    cities.filter((city)=> {
      return city.weatherName === e.target.value
    })
  }


  return (
    <div className="box">
      <div className="search">
        <input
          className="search-box"
          type="text"
          autoComplete="on"
          placeholder="Inserte nombre ciudad"
          onChange={handleChange}
          disabled={disable}
        ></input>
        <button onClick={getWeather} disabled={disable}>
          Obtén tiempo
        </button>
        <button onClick={sortedTemp} disabled={disable}>
          Ordenar
        </button>
        <select onChange={handleFilterChange} disabled={disable}>
          {climas.map((newClimas) => {
            return <option >{newClimas}</option>;
          })}
        </select>
      </div>
      <div className="container">
        {isLoading ? (
          ""
        ) : (
          <div>
            {cities.map((city, index) => {
              return (
                <Tiempo
                  key={index}
                  name={city.title}
                  image={city.icon_url}
                  clima={city.weather}
                />
              );
            })}
          </div>
        )}
        {isLoading ? (
          <div className="spinner">
            <ReactBootStrap.Spinner animation="border" />
          </div>
        ) : (
          ""
        )}

        <p>Media: {Math.round(avg)}ºC</p>
      </div>
    </div>
  );
}
export default Search;
