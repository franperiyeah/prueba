import React from 'react'
import './searcher.scss';

const Tiempo = ({name, image, clima, media}) => {
    return(
        <div className="time-container" >
        <div className="time-row" >
        <div className="time" >
        <img src={image} ></img>
        <h1>{name}</h1>
        <p className="time.symbol">{Math.round(clima)}ºC</p>
        </div>
        
        </div>

        </div>
    )
}

export default Tiempo